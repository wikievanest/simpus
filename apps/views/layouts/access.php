<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title><?php echo $template['title'];?></title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Admin, Dashboard, Bootstrap" />
  <link rel="shortcut icon" sizes="196x196" href="../assets/images/logo.png">
  
  <link rel="stylesheet" href="<?=base_url('public/assets/libs/bower/font-awesome/css/font-awesome.min.css');?>">
  <link rel="stylesheet" href="<?=base_url('public/assets/libs/bower/material-design-iconic-font/dist/css/material-design-iconic-font.min.css');?>">
  <link rel="stylesheet" href="<?=base_url('public/assets/libs/bower/animate.css/animate.min.css');?>">
  <link rel="stylesheet" href="<?=base_url('public/assets/css/bootstrap.css');?>">
  <link rel="stylesheet" href="<?=base_url('public/assets/libs/misc/toastr/toastr.min.css');?>">
  <link rel="stylesheet" href="<?=base_url('public/assets/css/core.css');?>">
  <link rel="stylesheet" href="<?=base_url('public/assets/css/misc-pages.css');?>">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,500,600,700,800,900,300">
    <script src="<?=base_url('public/assets/libs/bower/jquery/dist/jquery.js');?>"></script>
  <script src="<?=base_url('public/assets/libs/bower/breakpoints.js/dist/breakpoints.min.js');?>"></script>
  <script src="<?=base_url('public/assets/libs/misc/toastr/toastr.min.js');?>"></script>
</head>
<?php
    $notification = $this->session->flashdata('notification');
    if($notification != "") {
    ?>
    <script>
    $(document).ready(function() {
        toastr.options = {
          "debug": false,
          "positionClass": "toast-top-center",
          "onclick": null,
          "showDuration": "1000",
          "hideDuration": "1000",
          "timeOut": "5000",
          "extendedTimeOut": "1000",
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
        }
       toastr.<?php echo $notification['class'];?>("<?php echo $notification['message'];?>");
    });
    </script>
    <?php
    } else if(validation_errors() != false) {
    ?>
    <script>
    $(document).ready(function() {
        toastr.options = {
          "debug": false,
          "positionClass": "toast-top-center",
          "onclick": null,
          "showDuration": "1000",
          "hideDuration": "1000",
          "timeOut": "100000",
          "extendedTimeOut": "1000",
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
        }
        toastr.error('<?php echo str_replace(PHP_EOL, '', strip_tags(validation_errors()));;?>');
    });
    </script>
    <?php

    }
    ?>
<body class="simple-page">
  <div id="back-to-home">
    <a href="<?=base_url();?>" class="btn btn-outline btn-default"><i class="fa fa-home animated zoomIn"></i></a>
  </div>
   <?php echo $template['body']; ?>
</body>
</html>