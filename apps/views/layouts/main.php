
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>TUGAS DSS</title>

    <link rel="stylesheet" href="<?=base_url('public/assets/css/bootstrap.min.css');?>">
    <link rel="stylesheet" href="<?=base_url('public/assets/css/style.css');?>">
    <link rel="stylesheet" href="<?=base_url('public/assets/js/toastr/toastr.min.css');?>">
    <script src="<?=base_url('public/assets/js/jquery-3.4.1.min.js');?>"></script>
    <script src="<?=base_url('public/assets/js/toastr/toastr.min.js');?>"></script>
    <style>
    .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    @media (min-width: 768px) {
        .bd-placeholder-img-lg {
            font-size: 3.5rem;
        }
    }
    </style>
    <?php
    $notification = $this->session->flashdata('notification');
    if($notification != "") {
    ?>
    <script>
    $(document).ready(function() {
      toastr.options = {
        "closeButton": true,
        "debug": false,
        "positionClass": "toast-top-full-width",
        "onclick": null,
        "showDuration": "1000",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
      }
       toastr.<?php echo $notification['class'];?>("<?php echo $notification['message'];?>");
    });
    </script>
    <?php
      }
    ?>
</head>
<body>
    <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
        <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">TUGAS DSS</a>
        <h3 class="w-100" style="padding-left: 10px;font-size:1rem;color: #FFA500;padding-top: 10px">PROJECT ACTIVE: <?php echo $this->session->userdata('project_name');?></h3>
        <!-- <input class="form-control form-control-dark w-20" type="text" placeholder="Search" aria-label="Search"> -->
        <ul class="navbar-nav px-3">
            <li class="nav-item text-nowrap">
                <a class="nav-link" href="#">Sign out</a>
            </li>
        </ul>
    </nav>

    <div class="container-fluid">
        <div class="row">
            <nav class="col-md-2 d-none d-md-block bg-light sidebar">
                <div class="sidebar-sticky">
                    <ul class="nav flex-column">
                        <li class="nav-item">
                            <a class="nav-link active" href="#">
                                <span data-feather="home"></span>
                                Dashboard <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?=base_url('projects');?>">
                                <span data-feather="file"></span>
                                Projects
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?=base_url('parameters');?>">
                                <span data-feather="users"></span>
                                Parameters
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?=base_url('alternative');?>">
                                <span data-feather="shopping-cart"></span>
                                Alternative
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?=base_url('normalization');?>">
                                <span data-feather="bar-chart-2"></span>
                                Normalization
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?=base_url('preferences');?>">
                                <span data-feather="layers"></span>
                                Perferensi
                            </a>
                        </li>
                    </ul>

                    <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                        <span>Hasil Analisa</span>
                        <a class="d-flex align-items-center text-muted" href="#">
                            <span data-feather="plus-circle"></span>
                        </a>
                    </h6>
                    <ul class="nav flex-column mb-2">
                        <li class="nav-item">
                            <a class="nav-link" href="<?=base_url('result');?>">
                                <span data-feather="file-text"></span>
                                Kesimpulan
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>

            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
                <?php echo $template['body']; ?>
            </main>
        </div>
    </div>
    <script src="<?=base_url('public/assets/js/jquery-3.3.1.slim.min.js');?>"></script>
    <script src="<?=base_url('public/assets/js/bootstrap.bundle.min.js');?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.9.0/feather.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>
</body>
</html>
