<?php
/**
 * @FileInfo: /Users/wikie/Development/Sites/ustmappbruce/apps/libraries/Auth.php
 * @Author: wikie
 * @Email: wikieonline@gmail.com
 * @Date: 2016-07-20 12:09:49
 * @Last Modified by:   wikie
 * @Last Modified time: 2017-08-17 16:16:03
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth {

		public function is_admin() {
			$CI =& get_instance();
			$level = $CI->session->userdata('level');
			if($level <> 0) {
				return true;
			} 
		}

		public function is_login() {
			$CI =& get_instance();
			$is_login = $CI->session->userdata('is_login');
			if($is_login==false) {
				redirect(base_url('login'));
			}
		}

		public function check_access() {
			$CI =& get_instance();
			$level = $CI->session->userdata('level');
			$modul = $CI->uri->segment(1);

			if(($modul=='home') && ($level==1 || $level==5)) {
				return true;
			} else if(($modul=='dashboard') && ($level==1 || $level==5)) {
				return true;
			} else if(($modul=='searchpatient') && ($level==1 || $level==5)) {
				return true;
			} else if(($modul=='patients') && ($level==5)) {
				return true;
			} else if(($modul=='encounters') && ($level==1 || $level==5)) {
				return true;
			} else if(($modul=='patientbydate') && ($level==1 || $level==5)) {
				return true;
			} else if(($modul=='patientbystate') && ($level==1 || $level==5)) {
				return true;
			} else if(($modul=='patientbyencounter') && ($level==1 || $level==5)) {
				return true;
			} else if(($modul=='encounterbypatient') && ($level==1 || $level==5)) {
				return true;
			} else if(($modul=='encounterbyprovider') && ($level==1 || $level==5)) {
				return true;
			} else if(($modul=='encounterstoday') && ($level==1 || $level==5)) {
				return true;
			} else if(($modul=='doctors') && ($level==1 || $level==5)) {
				return true;
			} else if($modul=='logout') {
				return true;
			} else {
				redirect(base_url('home'));
			}

		}


}