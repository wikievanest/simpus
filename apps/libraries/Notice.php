<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class notice {

        public function get_message($status, $message) {
		$CI =& get_instance();
		$message = array(
				'message' => $message,
				'class' => $status);
		$CI->session->set_flashdata('notification', $message);
	}
}
