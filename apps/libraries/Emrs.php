<?php
/**
 * @FileInfo: /Users/wikie/Development/Sites/ustmapp7/apps/libraries/Emrs.php
 * @Author: wikie
 * @Email: wikieonline@gmail.com
 * @Date: 2016-07-20 12:09:49
 * @Last Modified by:   wikie
 * @Last Modified time: 2017-08-19 04:58:33
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Emrs {

		public static $emr;
        public static $client;
        public static $emr_number;
        
		function __construct() {
			$this->emr = 'EMR #7';
            $this->client = 'JOHN';
            $this->emr_number = 7;
		}

		public function get_emrs() {
    		$CI =& get_instance();
            $sql = "SELECT EMR_ID,EMR_NAME from EMRS
                    WHERE EMR_CLIENT='".$this->client."'";
            $query = $CI->db->query($sql);
            if ($query->num_rows() > 0) {
                $data = $query->result();

                return $data;
                $CI->db->close();
            }
            return false;
        }

}