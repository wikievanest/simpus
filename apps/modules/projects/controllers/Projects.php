<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Projects extends MY_Controller {

  var $title;
  var $desc;
  var $user_id;
  public function __construct() {
    $this->user_id = '4317f396-75d3-11e9-b607-48406b566f2c';
    $this->title = 'TUGAS DSS';
    $this->desc = 'TUGAS DSS';
    parent::__construct();
    $this->load->model("Project_qry",'project');
    // $this->auth->is_login();
  }

  public function index() {
    $data['projects'] = $this->project->get_data($this->user_id);
    $this->template
    ->title($this->title,$this->desc)
    ->set_layout('main')
    ->build('index',$data);
  }

  public function active() {
      $id = $this->uri->segment(3);
      $project = $this->project->get_project_by_id($id);
      if($project) {
          $member_sess = array(
            'project_id'  => $project->project_id,
            'project_name'     => $project->project_name,
          );
          $this->session->set_userdata($member_sess);
      }
      $this->notice->get_message('success','Project Berhasil diaktifkan!');
      redirect(base_url('projects'));

  }

  public function add() {
    $this->template
    ->title($this->title,$this->desc)
    ->set_layout('main')
    ->build('add',$data);
  }
  public function save() {
    $name = $this->input->post('name');
    $save = $this->project->save($name,$this->user_id);
    if($save) {
      $this->notice->get_message('success','Process Successfully!');
      redirect('projects');
    }

  }

  public function delete() {
    $id = $this->uri->segment(3);
    $save = $this->project->delete($id);
    if($save) {
      $this->notice->get_message('success','Project Berhasil dihapus!!');
      redirect('projects');
    }
  }


}
