<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
  <h1 class="h2">Buat Project Baru</h1>
</div>
<div>
<?php
    $submit = "projects/save";
          $attributes = array('role' => 'form'
              , 'id' => 'form_add', 'name' => 'form_add','class' => '', 'onSubmit' => 'document.getElementById(\'btn\').disabled=true;');
          echo form_open($submit,$attributes);
      ?>
  <div class="form-group">
  <input type="hidden" class="form-control" id="client_id" name="client_id">
  <div class="form-group">
    <label for="project_name" class="col-sm-3 control-label">Nama Project:</label>
    <div class="col-sm-6">
      <input type="text" class="form-control" id="name" name="name" placeholder="Project name" required="">
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-9 col-sm-offset-3">
      <button id="btn" type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan Data</button>
      <a href="<?=base_url('projects');?>" class="btn btn-default"><i class="fa fa-arrow-left"></i> Batal</a>
    </div>
  </div>
<?php echo form_close(); ?>
</div>
