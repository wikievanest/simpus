<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
  <h1 class="h2">Project List</h1>
  <div class="btn-toolbar mb-2 mb-md-0">
    <a href="<?=base_url('projects/add');?>" class="btn btn-md btn-primary">
      Tambah Data
    </a>
  </div>
</div>

<table id="responsive-datatable" class="table" data-plugin="DataTable" cellspacing="0" width="100%">
  <thead>
    <tr>
      <th style="width: 4%;text-align:center">ID</th>
      <th style="width: 10%">Nama Project</th>
      <th style="width: 10%;text-align:left">Alternative</th>
      <th style="width: 10%;text-align:left">Kriteria</th>
      <th style="width: 5%;text-align:left">Status</th>
      <th style="width: 10%;text-align:center">Aktifkan</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $i=1;
    foreach ($projects as $project) {
      if($project->project_id==$this->session->userdata('project_id')) {
        $status = '<span class="label label-success">Aktif</span>';
        
      } else {
        $status = '<span class="label label-default">Tidak Aktif</span>';
      }
      ?>
      <tr>
        <th style="width: 4%;text-align:center"><?php echo $i;?></th>
        <th style="width: 15%"><?php echo $project->project_name;?></th>
        <th style="width: 10%;text-align:left"><a href="<?=base_url('alternative');?>" class="btn btn-info btn-sm">Alternative</a></th>
        <th style="width: 10%;text-align:left"><a href="<?=base_url('parameters');?>" class="btn btn-success btn-sm">Kriteria</a></th>
        <th style="width: 5%;text-align:left"><?php echo $status;?></th>
        <th style="width: 10%;text-align:right">
          <a href="<?=base_url('projects/active').'/'.$project->project_id;?>" class="btn btn-success btn-sm">Aktifkan</a>
          &nbsp;
          <a href="<?=base_url('projects/delete').'/'.$project->project_id;?>" class="btn btn-danger btn-sm">Hapus</a>
      </th>
      </tr>
      <?php
      $i++;
    }
    ?>
  </tbody>
</tbody>
</table>
