<?php

class Project_qry extends CI_Model{
    //put your code here
    protected $emr;
    public function __construct() {
        parent::__construct();
    }

    public function save($name,$user_id) {
        $data = array(
            'project_name'=>$name,
            'status'=>0,
            'user_id'=>$user_id,
            'input_date'=>date('Y-m-d h:m:s'),
        );
        $this->db->set('project_id', 'UUID()', FALSE);
        $this->db->insert('projects',$data);
        return ($this->db->affected_rows() != 1) ? false : true;
    }

    public function get_data($user_id) {
        $sql = "SELECT a.project_id,
                	a.project_name,
                	a.user_id,
                	a.`status`
                FROM users b
                INNER JOIN projects a
                ON b.user_id = a.user_id
                WHERE a.user_id='$user_id'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $data = $query->result();


            return $data;
            $this->db->close();
        }
        return false;
    }

    public function get_project_by_id($id) {
        $query = $this->db->get_where('projects', array('project_id' => $id));
        if ($query->num_rows() > 0) {
            $data = $query->row();
            return $data;
            $this->db->close();
        }
        return false;
    }

    public function delete($id) {
        $query = $this->db->delete('projects', array('project_id' => $id)); 
        if ($query) {
            return true;
            $this->db->close();
        }
        return false;
    }
}
