<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Parameters extends MY_Controller {

  var $title;
  var $desc;
  var $user_id;
  public function __construct() {
    $this->user_id = '4317f396-75d3-11e9-b607-48406b566f2c';
    $this->title = 'TUGAS DSS';
    $this->desc = 'TUGAS DSS';
    parent::__construct();
    $this->load->model("Parameters_qry",'parameter');
    // $this->auth->is_login();
  }

  public function index() {
    $data['parameters'] = $this->parameter->get_data($this->user_id,$this->session->userdata('project_id'));
    $this->template
        ->title($this->title,$this->desc)
        ->set_layout('main')
        ->build('index',$data);
    
  }

  public function active() {
      $id = $this->uri->segment(3);
      $project = $this->parameter->get_project_by_id($id);
      if($project) {
          $member_sess = array(
            'project_id'  => $project->project_id,
            'project_name'     => $project->project_name,
          );
          $this->session->set_userdata($member_sess);
      }
      $this->notice->get_message('success','Project Berhasil diaktifkan!');
      redirect(base_url('projects'));

  }

  public function add() {
    $data['projects'] = $this->parameter->get_project();
    $this->template
    ->title($this->title,$this->desc)
    ->set_layout('main')
    ->build('add',$data);
  }


  public function save() {
    $project_id = $this->session->userdata('project_id');
    $bobot = $this->input->post('bobot');
    $name = $this->input->post('name');
    $tipe_param = $this->input->post('tipe_param');
    $save = $this->parameter->save($name,$bobot,$tipe_param,$project_id);
    if($save) {
      $this->notice->get_message('success','Parameter Berhasil disimpan!');
      redirect('parameters');
    }

  }

  public function delete() {
    $id = $this->uri->segment(3);
    $save = $this->parameter->delete($id);
    if($save) {
      $this->notice->get_message('success','Parameter Berhasil dihapus!!');
      redirect('parameters');
    }
  }

}
