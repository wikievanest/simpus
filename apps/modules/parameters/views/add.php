<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
  <h1 class="h2">Tambah Parameter Baru</h1>
</div>
<div>
<?php
    $submit = "parameters/save";
          $attributes = array('role' => 'form'
              , 'id' => 'form_add', 'name' => 'form_add','class' => '', 'onSubmit' => 'document.getElementById(\'btn\').disabled=true;');
          echo form_open($submit,$attributes);
      ?>
  <div class="form-group">
  <input type="hidden" class="form-control" id="id" name="id">
  <div class="form-group">
    <label for="param" class="col-sm-3 control-label">Nama Parameter:</label>
    <div class="col-sm-6">
      <input type="text" class="form-control" id="name" name="name" placeholder="" required="">
    </div>
  </div>
  <div class="form-group">
    <label for="bobot" class="col-sm-3 control-label">Bobot:</label>
    <div class="col-sm-3">
      <input type="number" class="form-control" id="bobot" name="bobot" placeholder="" required="">
    </div>
  </div>
  <div class="form-group">
    <label for="tipe_param" class="col-sm-3 control-label">Tipe Param:</label>
    <div class="col-sm-3">
      <select name="tipe_param" id="tipe_param" class="form-control">
        <option value="Cost">Cost (semakin murah semakin baik)</option>
        <option value="Benefit">Benefit (semakin tinggi semakin baik)</option>
      </select>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-9 col-sm-offset-3">
      <button id="btn" type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan Data</button>
      <a href="<?=base_url('parameters');?>" class="btn btn-default"><i class="fa fa-arrow-left"></i> Batal</a>
    </div>
  </div>
<?php echo form_close(); ?>
</div>
