<script type="text/javascript">
    $(document).ready(function() {
        $('#emr_id').multiselect();
    });
    $(function () {
        $('#join_date').datetimepicker({
            format: 'YYYY-MM-DD'
        });
    });
</script>

<section class="app-content">
    <div class="row">
      <!-- DOM dataTable -->
      <div class="col-md-12">
        <div class="widget">
          <header class="widget-header">
            <h4 class="widget-title">Edit client</h4>
          </header><!-- .widget-header -->
          <hr class="widget-separator">
          <div class="widget-body">
            
            <?php 
                $submit = "client/update";
                      $attributes = array('role' => 'form'
                          , 'id' => 'form_add', 'name' => 'form_add','class' => 'form-horizontal', 'onSubmit' => 'document.getElementById(\'btn\').disabled=true;');
                      echo form_open($submit,$attributes); 
                  ?>
              <div class="form-group">
                <label for="exampleTextInput1" class="col-sm-3 control-label">First Name:</label>
                <div class="col-sm-9">
                <input type="hidden" class="form-control" id="client_id" name="client_id" value="<?php echo $client->CLIENT_ID;?>">
                  <input type="text" class="form-control" id="first_name" name="first_name"  value="<?php echo $client->FIRST_NAME;?>">
                </div>
              </div>
              <div class="form-group">
                <label for="exampleTextInput1" class="col-sm-3 control-label">Last Name:</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="last_name" name="last_name" value="<?php echo $client->LAST_NAME;?>">
                </div>
              </div>
              <div class="form-group">
                <label for="exampleTextInput1" class="col-sm-3 control-label">Join Date:</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="join_date" name="join_date" value="<?php echo $client->JOIN_DATE;?>">
                </div>
              </div>
              <div class="form-group">
                <label for="email2" class="col-sm-3 control-label">Company Name:</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="company" name="company"  value="<?php echo $client->COMPANY;?>">
                </div>
              </div>
              <div class="form-group">
                <label for="email2" class="col-sm-3 control-label">Phone Number:</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="phone" name="phone" value="<?php echo $client->PHONE_NUMBER;?>">
                </div>
              </div>
              <div class="form-group">
                <label for="email2" class="col-sm-3 control-label">Email:</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="email" name="email" value="<?php echo $client->EMAIL_ADDR;?>">
                </div>
              </div>
              <div class="form-group">
                <label for="email2" class="col-sm-3 control-label">Address:</label>
                <div class="col-sm-9">
                  <textarea class="form-control" name="address" id="address" cols="10" rows="5"><?php echo $client->ADDRESS;?></textarea>
                </div>
              </div>
              <div class="form-group">
                <label for="email2" class="col-sm-3 control-label">City:</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="city" name="city" value="<?php echo $client->CITY;?>">
                </div>
              </div>
              <div class="form-group">
                <label for="email2" class="col-sm-3 control-label">State:</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="state" name="state" value="<?php echo $client->STATE;?>">
                </div>
              </div>
              <div class="form-group">
                <label for="email2" class="col-sm-3 control-label">Zip Code:</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="zipcode" name="zipcode" value="<?php echo $client->ZIP;?>">
                </div>
              </div>
              <div class="row">
                <div class="col-sm-9 col-sm-offset-3">
                  <button id="btn" type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save data</button>
                  <a href="/client" class="btn btn-default"><i class="fa fa-arrow-left"></i> Cancel</a>
                </div>
              </div>
            <?php echo form_close(); ?>
          </div><!-- .widget-body -->
        </div><!-- .widget -->
      </div><!-- END column -->
    </div><!-- .row -->
  </section><!-- .app-content -->