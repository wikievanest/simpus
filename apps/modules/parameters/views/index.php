<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
  <h1 class="h2">Parameters</h1>
  <div class="btn-toolbar mb-2 mb-md-0">
    <a href="<?=base_url('parameters/add');?>" class="btn btn-md btn-primary">
      Tambah Data
    </a>
  </div>
</div>

<table id="responsive-datatable" class="table" data-plugin="DataTable" cellspacing="0" width="100%">
  <thead>
    <tr>
      <th style="width: 4%;text-align:center">ID</th>
      <th style="width: 15%">Parameter</th>
      <th style="width: 5%">Bobot</th>
      <th style="width: 8%">Tipe Param</th>
      <th style="width: 10%;text-align:left">Project</th>
      <th style="width: 5%;text-align:center">Aksi</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $i=1;
    foreach ($parameters as $p) {
      ?>
      <tr>
        <th style="width: 4%;text-align:center"><?php echo $i;?></th>
        <th style="width: 15%"><?php echo $p->parameter_name;?></th>
        <th style="width: 5%"><?php echo $p->bobot;?> %</th>
        <th style="width: 5%;text-align:left"><?php echo $p->param_type;?></th>
        <th style="width: 10%;text-align:left"><?php echo $p->project_name;?></th>
        <th style="width: 5%;text-align:center"><a href="<?=base_url('parameters/delete').'/'.$p->parameter_id;?>" class="btn btn-danger btn-sm">Hapus</a></th>
      </tr>
      <?php
      $i++;
    }
    ?>
  </tbody>
</tbody>
</table>
