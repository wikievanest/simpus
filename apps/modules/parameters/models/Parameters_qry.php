<?php

class Parameters_qry extends CI_Model{
    public function __construct() {
        parent::__construct();
    }

    public function save($name,$bobot,$tipe_param,$project_id) {
        $data = array(
            'parameter_name'=>$name,
            'bobot'=>$bobot,
            'param_type'=>$tipe_param,
            'project_id'=>$project_id,
        );
        $this->db->set('parameter_id', 'UUID()', FALSE);
        $this->db->insert('parameters',$data);
        return ($this->db->affected_rows() != 1) ? false : true;
    }

    public function get_data($user_id,$project_id) {
        $sql = "SELECT b.parameter_id, 
                    a.project_id, 
                    a.project_name,
                    b.parameter_name, 
                    b.bobot,
                    b.param_type
                FROM projects a 
                INNER JOIN parameters b 
                ON b.project_id = a.project_id
                WHERE a.user_id='$user_id'
                AND b.project_id='$project_id'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $data = $query->result();


            return $data;
            $this->db->close();
        }
        return false;
    }

    public function get_project_by_id($id) {
        $query = $this->db->get_where('projects', array('project_id' => $id));
        if ($query->num_rows() > 0) {
            $data = $query->row();
            return $data;
            $this->db->close();
        }
        return false;
    }

    public function get_project() {
        $query = $this->db->get('projects');
        if ($query->num_rows() > 0) {
            $data = $query->result();
            return $data;
            $this->db->close();
        }
        return false;
    }

    public function delete($id) {
        $query = $this->db->delete('parameters', array('parameter_id' => $id)); 
        if ($query) {
            return true;
            $this->db->close();
        }
        return false;
    }
}
