<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
  <h1 class="h2">Hasil Perferensi</h1>
</div>

<table id="responsive-datatable" class="table" data-plugin="DataTable" cellspacing="0" width="100%">
<thead>
    <tr>
      <th>#</th>
      <?php foreach ($parameters as $p) {
      ?>
      <th style="width: 15%"><?php echo $p->parameter_name;?></th>
      <?php } ?>
    </tr>
  </thead>
  <tbody>
    <?php
    $i=1;
    foreach ($items as $p) {
      ?>
      <tr>
        <th style="width: 15%"><?php echo $p->alternative;?></th>
        <th style="width: 15%"><?php echo $p->AGAMA;?></th>
        <th style="width: 15%"><?php echo $p->FISIK;?></th>
        <th style="width: 15%"><?php echo $p->PENDIDIKAN;?></th>
        <th style="width: 15%"><?php echo $p->HARTA;?></th>

      </tr>
      <?php
      $i++;
    }
    ?>
  </tbody>
</tbody>
</table>
