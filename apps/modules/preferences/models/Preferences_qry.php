<?php

class Preferences_qry extends CI_Model{
    public function __construct() {
        parent::__construct();
    }

    public function get_data($project_id) {
        $this->db->reconnect();
        $sql = "call get_references('$project_id');";
        $query = $this->db->query($sql);
        $data = $query->result();
        mysqli_next_result( $this->db->conn_id );
        return $data;
    }

    
    public function proses_normalisasi($project_id) {
        $this->db->delete('reference', array('project_id' => $project_id)); 
        $sql = "INSERT INTO reference(reference_id,project_id,alternative_id,parameter_id,value)
                SELECT 
                UUID(),
                b.project_id,
                a.alternative_id,
                a.parameter_id,
                a.value * (b.bobot/100) as nilai
                FROM normalization a
                LEFT JOIN parameters b 
                ON b.parameter_id=a.parameter_id
                HAVING b.project_id='$project_id'";
        $query = $this->db->query($sql);
        if ($query) {
            return true;
            $this->db->close();
        }
        return false;
    }

    public function get_params($project_id) {
        $sql = "SELECT
                    parameters.parameter_name
                    FROM
                    parameters
                WHERE project_id='$project_id'";
        $query = $this->db->query($sql);
        if ($query) {
            $data = $query->result();
            return $data;
            $this->db->close();
        }
        return false;
    }
}
