<?php if ( ! defined('BASEPATH')) exit('No direct script auth allowed');


class Debug extends MY_Controller{
    protected $data = '';

    public function __construct() {
        parent::__construct();
        $this->data = array(
            'home' => site_url(),
            'msg_main' => "ERROR 403",
            'msg_detail' => "Request Page not found",
        );
    }

    public function index() {
        $this->err_404();
    }

    public function err_403() {
        $this->template
            ->title('Error 403','Access Forbidden')
            ->set_layout('main')
            ->build('debug/err_403',$this->data);
    }
}
