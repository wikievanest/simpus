<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Home extends MY_Controller {
    var $title;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Home_qry','home');
        // $this->auth->is_login();
        $this->title = 'Dashboard';
    }

    public function index() {
      $this->template
          ->title('Home','Sisrane')
          ->set_layout('main')
          ->build('index');
    }

}
