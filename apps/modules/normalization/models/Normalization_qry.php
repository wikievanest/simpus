<?php

class Normalization_qry extends CI_Model{
    public function __construct() {
        parent::__construct();
    }

    public function get_data($project_id) {
        $this->db->reconnect();
        $sql = "call get_normalization('$project_id');";
        $query = $this->db->query($sql);
        $data = $query->result();
        mysqli_next_result( $this->db->conn_id );
        return $data;
    }

    
    public function prose_normalisasi($project_id) {
        $this->db->delete('parameter_max', array('project_id' => $project_id)); 
        $this->db->delete('normalization', array('project_id' => $project_id)); 
        $sql = "INSERT INTO parameter_max(param_max_id,project_id,parameter_id,value)
                SELECT 
                UUID(),
                b.project_id,
                a.parameter_id,
                    CASE
                    WHEN b.param_type ='Cost' THEN MIN(a.value)
                    WHEN b.param_type ='Benefit' THEN MAX(a.value)
                END as value
                FROM alternative_value a
                LEFT JOIN parameters b 
                ON b.parameter_id=a.parameter_id
                GROUP BY a.parameter_id
                HAVING b.project_id='$project_id'";
        $sql2 = "INSERT INTO normalization(normalization_id,project_id,alternative_id,parameter_id,value)
                SELECT 
                UUID(),
                b.project_id,
                a.alternative_id,
                a.parameter_id,
                a.value/b.value as value
                FROM alternative_value a
                LEFT JOIN parameter_max b 
                ON b.parameter_id=a.parameter_id
                HAVING b.project_id='$project_id'";
        $this->db->query($sql);
        $query = $this->db->query($sql2);
        if ($query) {
            return true;
            $this->db->close();
        }
        return false;
    }

    public function get_params($project_id) {
        $sql = "SELECT
                    parameters.parameter_name
                    FROM
                    parameters
                WHERE project_id='$project_id'";
        $query = $this->db->query($sql);
        if ($query) {
            $data = $query->result();
            return $data;
            $this->db->close();
        }
        return false;
    }
}
