<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Normalization extends MY_Controller {

  var $title;
  var $desc;
  var $user_id;
  public function __construct() {
    $this->user_id = '4317f396-75d3-11e9-b607-48406b566f2c';
    $this->title = 'TUGAS DSS';
    $this->desc = 'TUGAS DSS';
    parent::__construct();
    $this->load->model("Normalization_qry",'normalize');
    // $this->auth->is_login();
  }

  public function index() {
    $this->normalize->prose_normalisasi($this->session->userdata('project_id'));
    $data['items'] = $this->normalize->get_data($this->user_id,$this->session->userdata('project_id'));
    $data['parameters'] = $this->normalize->get_params($this->session->userdata('project_id'));
    $this->template
        ->title($this->title,$this->desc)
        ->set_layout('main')
        ->build('index',$data);
    
  }
}
