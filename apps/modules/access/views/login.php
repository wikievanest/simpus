<div class="simple-page-wrap">
  <div class="simple-page-logo animated swing">
    <a href="#">
      <span><i class="fa fa-app"></i></span>
      <!-- <span><strong>USTM Operational</strong></span> -->
    </a>
  </div><!-- logo -->
  <div class="simple-page-form animated flipInY" id="login-form" style="margin-top: -30px">
  <img src="<?php echo base_url('public/img/logo.png');?>">
  <hr>
<h4 class="form-title m-b-xl text-center">Sign In with your account</h4>
<hr>
<?php 
$submit = "auth";
    $attributes = array('role' => 'form'
        , 'id' => 'form_add', 'name' => 'form_add', 'onSubmit' => 'document.getElementById(\'btn\').disabled=true;');
    echo form_open($submit,$attributes); 
?>
  <div class="form-group">
    <input id="email" type="text" name ="email" class="form-control" placeholder="Email" value="<?php echo set_value('email');?>" autofocus>
  </div>

  <div class="form-group">
    <input id="password" type="password" name="password" class="form-control" placeholder="Password" value="<?php echo set_value('password');?>">
  </div>
  <button type="submit" class="btn btn-primary" id="btn"><i class="zmdi zmdi-sign-in"></i> SIGN IN</button>
<?php echo form_close(); ?>
</div><!-- #login-form -->

</div><!-- .simple-page-wrap -->