<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Access extends MY_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Access_qry','access');
    }

    public function register() {
      $this->template
          ->title('Registrasi User','DSS')
          ->set_layout('public')
          ->build('register');
    }

    public function index() {
        $this->login();
    }

    public function login() {
        $is_login = $this->session->userdata('is_login');
        if($is_login == true) {
            redirect(base_url('home'));
        }
        $this->template
            ->title('Account Login','')
            ->set_layout('access')
            ->build('login');
    }

    public function auth() {
        $is_login = $this->session->userdata('is_login');
        if($is_login == true) {
            redirect(base_url('home'));
        }
        $this->_validation();
        if($this->form_validation->run() == false) {
            $this->login();
        } else {
            $email = $this->security->xss_clean($this->input->post('email'));
            $password = $this->security->xss_clean($this->input->post('password'));
            $login = $this->access->login($email,$password);
            // Invalid Password
            if($login==0) {
                $this->notice->get_message('error','Password invalid!');
                $this->login();
            } elseif($login==1) {
                redirect(base_url('home'));
            // User not Active
            } elseif($login==9) {
                $this->notice->get_message('error','You dont have access to this page!');
                $this->login();
            // User not have access to this page
            }

        }
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect(base_url());
    }

    public function _validation() {
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required');

    }


}
