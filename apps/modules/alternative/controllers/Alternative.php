<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Alternative extends MY_Controller {

  var $title;
  var $desc;
  var $user_id;
  public function __construct() {
    $this->user_id = '4317f396-75d3-11e9-b607-48406b566f2c';
    $this->title = 'TUGAS DSS';
    $this->desc = 'TUGAS DSS';

    parent::__construct();
    $this->load->model("Alternative_qry",'alt');
    // $this->auth->is_login();
  }

  public function index() {
    $data['alternative'] = $this->alt->get_data($this->user_id,$this->session->userdata('project_id'));
    $this->template
        ->title($this->title,$this->desc)
        ->set_layout('main')
        ->build('index',$data);
    
  }

  public function active() {
      $id = $this->uri->segment(3);
      $project = $this->alt->get_project_by_id($id);
      if($project) {
          $member_sess = array(
            'project_id'  => $project->project_id,
            'project_name'     => $project->project_name,
          );
          $this->session->set_userdata($member_sess);
      }
      $this->notice->get_message('success','Project Berhasil diaktifkan!');
      redirect(base_url('projects'));

  }

  public function add() {
    $data['projects'] = $this->alt->get_project();
    $this->template
    ->title($this->title,$this->desc)
    ->set_layout('main')
    ->build('add',$data);
  }

  public function parameter() {
    $alt_id = $this->uri->segment(3);
    $data['alternative'] = $this->alt->get_alternative_by_id($alt_id);

    $data['params'] = $this->alt->get_parameter_by_project($this->session->userdata('project_id'),$alt_id);
    $this->template
    ->title($this->title,$this->desc)
    ->set_layout('main')
    ->build('parameter',$data);
  }

  public function save_parameter() {
    $param_id = $this->input->post('param_id');
    $bobot = $this->input->post('bobot');
    $alter_id = $this->input->post('id');
    $save = $this->alt->save_param($param_id,$bobot,$alter_id);
    if($save) {
      $this->notice->get_message('success','Nilai Parameter Berhasil disimpan!');
      redirect('alternative');
    }
  }


  public function save() {
    $project_id = $this->session->project_id;
    $name = $this->input->post('name');
    $save = $this->alt->save($name,$project_id);
    if($save) {
      $this->notice->get_message('success','Alternative Berhasil disimpan!');
      redirect('alternative');
    }

  }

  public function delete() {
    $id = $this->uri->segment(3);
    $save = $this->alt->delete($id);
    if($save) {
      $this->notice->get_message('success','Alternative Berhasil dihapus!!');
      redirect('alternative');
    }
  }

}
