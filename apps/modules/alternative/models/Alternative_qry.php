<?php

class Alternative_qry extends CI_Model{
    public function __construct() {
        parent::__construct();
    }

    public function save($name,$project_id) {
        $data = array(
            'alternative'=>$name,
            'project_id'=>$project_id,
            'input_date'=>date('Y-m-d h:m:s'),
        );
        $this->db->set('alternative_id', 'UUID()', FALSE);
        $this->db->insert('alternative',$data);
        return ($this->db->affected_rows() != 1) ? false : true;
    }

    public function save_param($params,$values,$alter_id) {

        $data = array_combine($params,$values);

        $datas = [];
        $i = 1;

        foreach ($data as $key => $value) {
            $datas[$i] = array(
                'alternative_id'=>$alter_id,
                'parameter_id'=>$key,
                'value'=>$value
            );
            $i++;
        }

        
        $this->db->delete('alternative_value', array('alternative_id' => $alter_id)); 
        $this->db->insert_batch('alternative_value',$datas);
        return ($this->db->affected_rows() < 1) ? false : true;
    }

    public function get_data($user_id,$project_id) {
        $sql = "SELECT b.alternative_id, 
                    a.project_id, 
                    b.alternative, 
                    a.project_name
                FROM projects a 
                INNER JOIN alternative b 
                ON b.project_id = a.project_id
                WHERE a.user_id='$user_id'
                AND b.project_id='$project_id'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $data = $query->result();
            return $data;
            $this->db->close();
        }
        return false;
    }

    public function get_project_by_id($id) {
        $query = $this->db->get_where('projects', array('project_id' => $id));
        if ($query->num_rows() > 0) {
            $data = $query->row();
            return $data;
            $this->db->close();
        }
        return false;
    }

    public function get_parameter_by_project($project_id,$alt_id) {
        $sql = "SELECT alternatif.alternative_id,	
                params.parameter_id,params.parameter_name,alternatif.`value`
                FROM (SELECT
                a.parameter_id,
                a.parameter_name
                FROM
                parameters a
                WHERE project_id='$project_id') as params 
                LEFT OUTER JOIN (SELECT b.alternative_value_id,
                        b.parameter_id,
                        b.alternative_id, 
                        b.`value`
                FROM alternative_value b 
                WHERE b.alternative_id='$alt_id') as alternatif 
                ON alternatif.parameter_id=params.parameter_id";
                
        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $data = $query->result();
            return $data;
            $this->db->close();
        }
        return false;
    }

    public function get_alternative_by_id($id) {
        $query = $this->db->get_where('alternative', array('alternative_id' => $id));
        if ($query->num_rows() > 0) {
            $data = $query->row();
            return $data;
            $this->db->close();
        }
        return false;
    }

    public function get_project() {
        $query = $this->db->get('projects');
        if ($query->num_rows() > 0) {
            $data = $query->result();
            return $data;
            $this->db->close();
        }
        return false;
    }

    public function delete($id) {
        $query = $this->db->delete('alternative', array('alternative_id' => $id)); 
        if ($query) {
            return true;
            $this->db->close();
        }
        return false;
    }
}
