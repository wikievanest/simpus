<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Isi Nilai Alternative <?php echo $alternative->alternative;?></h1>
</div>
<div>
    <?php
    $submit = "alternative/save_parameter";
    $attributes = array(
        'role' => 'form', 'id' => 'form_add', 'name' => 'form_add', 'class' => 'form-horizontal', 'onSubmit' => 'document.getElementById(\'btn\').disabled=true;'
    );
    echo form_open($submit, $attributes);
    ?>
    <?php
    ?>
    <?php foreach ($params as $param) {
        ?>
        <div class="form-group">
            
            <div class="form-group">
                <label for="param" class="col-sm-5 control-label"><?php echo $param->parameter_name;?> :</label>
                <input type="hidden" class="form-control" id="param_id" name="param_id[]" value="<?php echo $param->parameter_id;?>">
                <div class="col-sm-3">
                    <input type="number" class="form-control" id="bobot" name="bobot[]" value="<?php echo $param->value;?>" required="">
                </div>
            </div>
        <?php } ?>
        <input type="hidden" class="form-control" id="id" name="id" value="<?php echo $alternative->alternative_id;?>">
        <div class="form-group">
            <div class="col-sm-9 col-sm-offset-3">
                <button id="btn" type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan Data</button>
                <a href="<?= base_url('alternative'); ?>" class="btn btn-default"><i class="fa fa-arrow-left"></i> Batal</a>
            </div>
        </div>
        <?php echo form_close(); ?>