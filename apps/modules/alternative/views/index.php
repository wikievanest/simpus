<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
  <h1 class="h2">Alternative</h1>
  <div class="btn-toolbar mb-2 mb-md-0">
    <a href="<?=base_url('alternative/add');?>" class="btn btn-md btn-primary">
      Tambah Data
    </a>
  </div>
</div>

<table id="responsive-datatable" class="table" data-plugin="DataTable" cellspacing="0" width="100%">
  <thead>
    <tr>
      <th style="width: 4%;text-align:center">ID</th>
      <th style="width: 15%">Alternative</th>
      <th style="width: 10%;text-align:left">Project</th>
      <th style="width: 10%;text-align:center">Parameter</th>
      <th style="width: 5%;text-align:center">Aksi</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $i=1;
    foreach ($alternative as $a) {
      ?>
      <tr>
        <th style="width: 4%;text-align:center"><?php echo $i;?></th>
        <th style="width: 15%"><?php echo $a->alternative;?></th>
        <th style="width: 5%;text-align:left"><?php echo $a->project_name;?></th>
        <th style="width: 5%;text-align:center"><a href="<?=base_url('alternative/parameter').'/'.$a->alternative_id;?>" class="btn btn-info btn-sm">Parameter</a></th>
        <th style="width: 5%;text-align:center"><a href="<?=base_url('alternative/delete').'/'.$a->alternative_id;?>" class="btn btn-danger btn-sm">Hapus</a></th>
      </tr>
      <?php
      $i++;
    }
    ?>
  </tbody>
</tbody>
</table>
