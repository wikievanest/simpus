<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
  <h1 class="h2">Hasil Analisa</h1>
</div>

<table id="responsive-datatable" class="table" data-plugin="DataTable" cellspacing="0" width="100%">
<thead>
    <tr>
    <th>#</th>
    <th>Alternative</th>
    <th>Hasil</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $i=1;
    foreach ($items as $p) {
      ?>
      <tr>
      <th><?php echo $i;?></th>
        <th><?php echo $p->alternative;?></th>
        <th><?php echo $p->hasil;?></th>

      </tr>
      <?php
      $i++;
    }
    ?>
  </tbody>
</tbody>
</table>
