<?php

class Result_qry extends CI_Model{
    public function __construct() {
        parent::__construct();
    }

    public function get_data($project_id) {
        $this->db->reconnect();
        $sql = "SELECT
                a.alternative,
                ROUND(SUM(b.`value`),2) AS hasil
                FROM
                alternative AS a
                JOIN reference AS b
                ON a.alternative_id = b.alternative_id
                GROUP BY a.alternative
                ORDER BY hasil desc
                ;";
        $query = $this->db->query($sql);
        $data = $query->result();
        return $data;
    }

}
